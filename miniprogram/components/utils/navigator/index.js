Component({
    /**
     * 组件的属性列表
     */
    properties: {
        targetType: String,
        targetUrl: String
    },

    /**
     * 组件的初始数据
     */
    data: {
        _targetUrl: ''
    },

    attached: function() {
        var pages = getCurrentPages();
        var currentPage = pages[pages.length - 1];
        var route = currentPage.route;

        if (!this.data.targetType) {
            this.setData({
                targetType: "page"
            });
        }

        if (route === "pages/index/index") {
            const targetUrl = this.data.targetUrl;
            this.setData({
                _targetUrl: targetUrl.replace("pages/index/index", "pages/index2/index"),
                openType: 'navigate'
            })
        } else {
            const targetUrl = this.data.targetUrl;
            this.setData({
                _targetUrl: targetUrl.replace("pages/index/index", "pages/index2/index"),
                openType: 'navigate'
            })
        }
    },

    ready() {
        console.log(this.data.targetUrl)
    },

    /**
     * 组件的方法列表
     */
    methods: {
        makePhoneCall(e) {
            wx.makePhoneCall({
                phoneNumber: e.currentTarget.dataset.targetUrl
            });
        },
        toggleTopPopup() {
            this.setData({
                addMyMiniappPopup: !this.data.addMyMiniappPopup
            });
        }
    }
});